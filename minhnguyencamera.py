import signal, os, subprocess
import RPi.GPIO as GPIO
from time import sleep
from sh import gphoto2 as gp

GPIO.setmode(GPIO.BCM)
infared = 27
#output = 17
GPIO.setup(infared,GPIO.IN)
#GPIO.setup(output,GPIO.OUT)

def killgphoto2Process():
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()

    for line in out.splitlines():
            if b'gvfsd-gphoto2' in line:
                    pid = int(line.split(None,1)[0])
                    os.kill(pid, signal.SIGKILL)

triggerCommand = ["--trigger-capture"]

def captureImages():
    gp(triggerCommand)
    sleep(3)

killgphoto2Process()
print("Time Lapse Running!  Press CTRL+C to exit")
try:
	while True:
		if GPIO.input(infared):
			print("You got moves!")
			captureImages()
except KeyboardInterrupt:
	GPIO.cleanup()

