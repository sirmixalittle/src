#!/usr/lib/python3
import os
import time
import datetime
import RPi.GPIO as GPIO
import glob
import MySQLdb
from time import strftime

GPIO.setmode(GPIO.BCM)

#Awfa Al-Rakabi Lab 8
#Variables for MySQL
db=MySQLdb.connect(host="localhost",user="root",passwd="pswd",db="wordpress")
#db=PyMySQL.connect(host="localhost",user="root",passwd="pswd",db="wordpress")
cur = db.cursor()

def dateTime():
	secs = float(time.time())
	secs = secs*1000
	return secs
def response():
	resp = bool(GPIO.setup(int(18),GPIO.IN))
	return resp

secs = dateTime()
resp = response()

sql = ("""INSERT INTO wp_gpio (datetime,response) VALUES (%s,%s)""", (secs, resp))

try:
	print ("Writing to database...")
	cur.execute(*sql)
	db.commit()
	print ("Write complete")
except:
	db.rollback()
	print ("We have a problem")

cur.close()
db.close()

print (secs)
print (resp)
