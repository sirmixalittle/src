#Awfa Al-Rakabi Lab6 | Final Project 
from flask import Flask, render_template
import datetime
import RPi.GPIO as GPIO
app = Flask (__name__)

GPIO.setmode(GPIO.BCM)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/readPin/18')
def readPin():
    try:
        GPIO.setup(int(18),GPIO.IN)
        if GPIO.input(int(18))==True:
            response = "Pin number 18 is high!"
        else:
            response = "Pin number 18 is low!"
    except:
        response = "There was an error reading pin 18."
    templateData={
        'title': 'Status of Pin 18',
        'response': response
    }
    return render_template('pin.html',**templateData)

@app.route('/hello/<name>')
def hello(name):
    return render_template('page.html', name=name)
@app.route('/sirMixer')
def sirMixer():
    now = datetime.datetime.now()
    try:
        GPIO.setup(int(2),GPIO.IN)      #Pin 2 used for Camera
        if GPIO.input(int(2))==True:
            response = "Camera on!"
        else:
            response = "Camera off!"
    except:
        response = "There was an error reading the camera."
    timeString = now.strftime("%Y-%m-%d %H:%M")
    templateData={
        'title':'Sound Mixing',
        'time':timeString,
        'response': response
        }
    return render_template('sirMixer.html', **templateData)
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
