import signal, os, subprocess
import RPi.GPIO as GPIO
import signal
import time
from time import sleep
from sh import gphoto2 as gp

GPIO.setmode(GPIO.BCM)
button = 21
led1 = 13
led2 = 19
led3 = 26
buzz = 6
GPIO.setup(button,GPIO.IN)
GPIO.setup(led1,GPIO.OUT)
GPIO.setup(led2,GPIO.OUT)
GPIO.setup(led3,GPIO.OUT)
GPIO.setup(buzz,GPIO.OUT)

def killgphoto2Process():
    p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
    out, err = p.communicate()

    for line in out.splitlines():
            if b'gvfsd-gphoto2' in line:
                    pid = int(line.split(None,1)[0])
                    os.kill(pid, signal.SIGKILL)

triggerCommand = ["--trigger-capture"]

def captureImages():
    gp(triggerCommand)
    sleep(3)

killgphoto2Process()
GPIO.output(led1, GPIO.LOW)
GPIO.output(led2, GPIO.LOW)
GPIO.output(led3, GPIO.LOW)
GPIO.output(buzz, GPIO.LOW)
print("PhotoBooth Running!  Press CTRL+C to exit")
try:
    while True:
        if GPIO.input(button):
            print("Smile! Taking a pic in 3")
            GPIO.output(led1, GPIO.HIGH)
            GPIO.output(buzz, GPIO.HIGH)
            sleep(.2)
            GPIO.output(buzz, GPIO.LOW)
            sleep(1)
            print("2")
            GPIO.output(led1, GPIO.HIGH)
            GPIO.output(led2, GPIO.HIGH)
            GPIO.output(buzz, GPIO.HIGH)
            sleep(.4)
            GPIO.output(buzz, GPIO.LOW)
            sleep(1)
            print("1")
            GPIO.output(led1, GPIO.HIGH)
            GPIO.output(led2, GPIO.HIGH)
            GPIO.output(led3, GPIO.HIGH)
            print("Say Cheese!")
            GPIO.output(buzz, GPIO.HIGH)
            sleep(1)
            GPIO.output(buzz, GPIO.LOW)
            captureImages()
            GPIO.output(led1, GPIO.LOW)
            GPIO.output(led2, GPIO.LOW)
            GPIO.output(led3, GPIO.LOW)
except KeyboardInterrupt:
    GPIO.cleanup()