#Awfa Al-Rakabi Lab9
import RPi.GPIO as GPIO
from time import sleep

ledPin = 21

GPIO.setmode(GPIO.BCM)
GPIO.setup(ledPin,GPIO.OUT)

pwm = GPIO.PWM(ledPin,100)
pwm.start(0)
try:
	while 1:
		for x in range(100):
			pwm.ChangeDutyCycle(x)
			sleep(0.01)

		for x in range(100,0,-1):
			pwm.ChangeDutyCycle(x)
			sleep(0.01)
except KeyboardInterrupt:
	pass
pwm.stop()
GPIO.cleanup()

